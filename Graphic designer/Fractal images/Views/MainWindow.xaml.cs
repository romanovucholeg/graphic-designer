﻿using Fractal_images.ViewModels;
using Fractal_images.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Fractal_images
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            FirstRowListView.SelectionChanged += FirstRowListView_ItemActivate;

        }

        private void FirstRowListView_ItemActivate(Object sender, EventArgs e)
        {
            if(FirstRowListView.SelectedIndex != -1)
            {
                switch (FirstRowListView.SelectedIndex)
                {
                    case 0:
                        FractalImagesViewModel fractalImagesViewModel = new FractalImagesViewModel();
                        FractalImagesWindow fractalImageWindow = new();
                        fractalImageWindow.DataContext = fractalImagesViewModel;
                        fractalImageWindow.ShowDialog();
                        break;
                        case 1:
                        ColorSchemesViewModel colorSchemesViewModel = new ColorSchemesViewModel();
                        ColorSchemesWindow colorSchemesWindow = new ColorSchemesWindow();
                        colorSchemesWindow.DataContext = colorSchemesViewModel;
                        colorSchemesWindow.ShowDialog();
                        break;
                        case 2:
                        MovingImagesViewModel movingImagesViewModel = new MovingImagesViewModel();
                        MovingImagesWindow movingImagesWindow = new MovingImagesWindow();
                        movingImagesWindow.DataContext = movingImagesViewModel;
                        movingImagesWindow.ShowDialog();
                        break;
                        case 3:
                        EducationalViewModel educationalViewModel = new EducationalViewModel();
                        EducationalWindow educationalWindow = new EducationalWindow();
                        educationalWindow.DataContext = educationalViewModel;
                        educationalWindow.ShowDialog();
                        break;
                }
                FirstRowListView.SelectedItem = null;
            }
        }
    }
}
