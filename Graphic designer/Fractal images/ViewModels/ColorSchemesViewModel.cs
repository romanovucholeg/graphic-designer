﻿using Fractal_images.Commands;
using Fractal_images.Models;
using Fractal_images.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Fractal_images.ViewModels
{
    internal class ColorSchemesViewModel : ViewModelBase
    {
        private CustomImage customImage;

        public CustomImage CustomImage
        {
            get { return customImage; }
            set
            {
                customImage = value;
                OnPropertyChanged(nameof(CustomImage));
            }
        }

        public ColorSchemesViewModel()
        {
            customImage = new CustomImage();
        }


        private DelegateCommand сhangeSaturationCommand;

        public ICommand ChangeSaturationCommand
        {
            get
            {
                if (сhangeSaturationCommand == null)
                {
                    сhangeSaturationCommand = new DelegateCommand(CustomImage.СhangeSaturation);
                }
                return сhangeSaturationCommand;
            }
        }

        private DelegateCommand loadImageCommand;

        public ICommand LoadImageCommand
        {
            get
            {
                if (loadImageCommand == null)
                {
                    loadImageCommand = new DelegateCommand(CustomImage.LoadImage);
                }
                return loadImageCommand;
            }
        }
    }
}
