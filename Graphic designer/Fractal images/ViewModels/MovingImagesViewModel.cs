﻿using Fractal_images.Commands;
using Fractal_images.Models;
using Fractal_images.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Fractal_images.ViewModels
{
    internal class MovingImagesViewModel : ViewModelBase
    {

        Parallelogram parallelogram;

        public Parallelogram Parallelogram
        {
            get { return parallelogram; }
            set
            {
                parallelogram = value;
                OnPropertyChanged(nameof(Parallelogram));
            }
        }

        public MovingImagesViewModel()
        {
            Parallelogram = new Parallelogram();
        }


    }
}
