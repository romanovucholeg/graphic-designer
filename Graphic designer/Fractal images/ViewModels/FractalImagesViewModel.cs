﻿using Fractal_images.Commands;
using Fractal_images.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace Fractal_images.ViewModels
{

    public enum Shape
    {
        Triangle,
        Rectangle
    }

    public class FractalImagesViewModel : ViewModelBase
    {

        public FractalImagesViewModel()
        {
        }

        private List<PointCollection> steps = new List<PointCollection>();

        private double canvasWidth = 980;


        private double canvasHeight = 850;


        private Shape currentShape = Shape.Triangle;

        public Shape CurrentShape
        {
            get { return currentShape; }
            set
            {
                currentShape = value;
                OnPropertyChanged(nameof(CurrentShape));
            }
        }

        private int curIteration = -1;

        public int CurIteration { 
            get { return curIteration; } 
            set { curIteration = value;
                OnPropertyChanged(nameof(CurIteration));
            } 
            }

        private PointCollection curIterationPoints = new PointCollection();
        public PointCollection CurIterationPoints
        {
            get { return curIterationPoints; }
            set
            {
                curIterationPoints = value;
                OnPropertyChanged(nameof(CurIterationPoints));
            }
        }

        private double centerX = 490;
        public double CenterX
        {
            get { return centerX; }
            set
            {
                centerX = value;  
                OnPropertyChanged(nameof(CenterX));
            }
        }

        private double centerY = 425;
        public double CenterY
        {
            get { return centerY; }
            set
            {
                centerY = value;
                OnPropertyChanged(nameof(CenterY));
            }
        }

        private int lineSegment1SizeCoefficient = 1;

        public int LineSegment1SizeCoefficient
        {
            get { return lineSegment1SizeCoefficient; }
            set
            {
                lineSegment1SizeCoefficient = value;
                OnPropertyChanged(nameof(lineSegment1SizeCoefficient));
            }
        }

        private int lineSegment2SizeCoefficient = 1;

        public int LineSegment2SizeCoefficient
        {
            get { return lineSegment2SizeCoefficient; }
            set
            {
                lineSegment2SizeCoefficient = value;
                OnPropertyChanged(nameof(lineSegment2SizeCoefficient));
            }
        }

        private int lineSegment3SizeCoefficient = 1;

        public int LineSegment3SizeCoefficient
        {
            get { return lineSegment3SizeCoefficient; }
            set
            {
                lineSegment3SizeCoefficient = value;
                OnPropertyChanged(nameof(lineSegment3SizeCoefficient));
            }
        }

        private int iterationCount = 3;

        public int IterationCount
        {
            get { return iterationCount; }
            set
            {
                iterationCount = value;
                OnPropertyChanged(nameof(iterationCount));
            }
        }

        private int iterationsToAddCount = 1;
        public int IterationsToAddCount
        {
            get { return iterationsToAddCount; }
            set
            {
                iterationsToAddCount = value;
                OnPropertyChanged(nameof(iterationsToAddCount));
            }
        }

        private int iterationsToSubstractCount = 1;
        public int IterationsToSubstractCount
        {
            get { return iterationsToSubstractCount; }
            set
            {
                iterationsToSubstractCount = value;
                OnPropertyChanged(nameof(iterationsToSubstractCount));
            }
        }

        private DelegateCommand fractalIterationCommand;

        public ICommand BuildFractalCommand
        {
            get
            {
                if (fractalIterationCommand == null)
                {
                    fractalIterationCommand = new DelegateCommand(BuildFractal);
                }
                return fractalIterationCommand;
            }
        }


        private DelegateCommand addIterationsCommand;

        public ICommand AddIterationsCommand
        {
            get
            {
                if (addIterationsCommand == null)
                {
                    addIterationsCommand = new DelegateCommand(AddIterations);
                }
                return addIterationsCommand;
            }
        }

        private DelegateCommand substractIterationsCommand;

        public ICommand SubstractIterationsCommand
        {
            get
            {
                if (substractIterationsCommand == null)
                {
                    substractIterationsCommand = new DelegateCommand(SubstractIterations);
                }
                return substractIterationsCommand;
            }
        }

        private void BuildFractal()
        {
            CurIterationPoints.Clear();
            steps.Clear();
            CurIteration = 0;
            //Точки для базової фігури(1-ша ітерація)
            switch (CurrentShape)
            {
                case Shape.Triangle:
                    CurIterationPoints.Add(new Point(0, 0));
                    CurIterationPoints.Add(new Point(160, 0));
                    CurIterationPoints.Add(new Point(80, 140));
                    break;
                    case Shape.Rectangle:
                    CurIterationPoints.Add(new Point(0, 0));
                    CurIterationPoints.Add(new Point(160, 0));
                    CurIterationPoints.Add(new Point(160, 160));
                    CurIterationPoints.Add(new Point(0, 160));
                    break;
            }
            //зробимо зміщення для точок, щоб розташувати у вибраному користувачем місці
            PointCollection pointsCopy = new();
            double baseDistance = Math.Sqrt(Math.Pow(CurIterationPoints.ElementAt(0).X - CurIterationPoints.ElementAt(1).X, 2) + Math.Pow(CurIterationPoints.ElementAt(0).Y - CurIterationPoints.ElementAt(1).Y, 2));//довжина базового відрізка
            foreach (Point point in CurIterationPoints)
            {
                pointsCopy.Add(new Point(point.X + (CenterX- baseDistance/2), point.Y + (CenterY- baseDistance/2)));
            }
            CurIterationPoints = pointsCopy;
            steps.Add(CurIterationPoints);
            for(int i = 0; i < iterationCount-1; ++i)
            {
                ++CurIteration;
                FractalIteration();
            }
            //перевірка чи не виходить фрактал за межі полотна
            foreach(Point point in steps.Last())
            {
                if(point.X < 0 || point.X > canvasWidth || point.Y < 0 || point.Y > canvasHeight)
                {
                    MessageBox.Show("Фігура виходить за межі координатної сітки. Змініть, будь ласка, її центр.");
                    CurIterationPoints.Clear();
                    steps.Clear();
                    CurIterationPoints = new PointCollection();
                    CurIteration = 0;
                    break;
                }
            }
        }


        private void FractalIteration()
        {
                PointCollection pointsTemp = new();
            foreach (var point in CurIterationPoints)
            {
                Point nextPoint = new Point(); 
                if (CurIterationPoints.IndexOf(point) == CurIterationPoints.Count-1)//якщо це остання точка, то за наступну беремо першу
                {
                    nextPoint = CurIterationPoints[0];
                } else
                {
                    nextPoint = CurIterationPoints[CurIterationPoints.IndexOf(point) + 1];
                }
                pointsTemp.Add(point);
                double firstSegmentLambda = Convert.ToDouble(lineSegment1SizeCoefficient) / (lineSegment2SizeCoefficient+lineSegment3SizeCoefficient);//коефіцієнт поділу першого сегменту до інших двох частин для обчислення першої точки, де поділяється відрізок
                Point dividePoint1 = new Point((point.X + firstSegmentLambda * nextPoint.X) / (1 + firstSegmentLambda), (point.Y + firstSegmentLambda * nextPoint.Y) / (1 + firstSegmentLambda));//перша точка, де поділяється відрізок за відношенням
                double secondSegmentLambda = Convert.ToDouble(lineSegment3SizeCoefficient) / (lineSegment1SizeCoefficient + lineSegment2SizeCoefficient);//коефіцієнт поділу другого сегменту до інших двох частин для обчислення другої точки, де поділяється відрізок
                Point dividePoint2 = new Point((nextPoint.X + secondSegmentLambda * point.X) / (1 + secondSegmentLambda), (nextPoint.Y + secondSegmentLambda * point.Y) / (1 + secondSegmentLambda));//друга точка, де поділяється відрізок за відношенням
                
                pointsTemp.Add(dividePoint1);
                switch (CurrentShape)
                {
                    case Shape.Triangle:
                        pointsTemp.Add(new Point((dividePoint2.X - dividePoint1.X) * 0.5 - (dividePoint2.Y - dividePoint1.Y) * (-1) * Math.Sqrt(3) / 2 + dividePoint1.X,
                                                 (dividePoint2.X - dividePoint1.X) * (-1) * Math.Sqrt(3) / 2 + (dividePoint2.Y - dividePoint1.Y) * 0.5 + dividePoint1.Y));//вершина трикутника, що добудовується

                        break;
                    case Shape.Rectangle:
                        double distanceBetweenDividePoints = Math.Sqrt(Math.Pow(dividePoint1.X-dividePoint2.X, 2)+ Math.Pow(dividePoint1.Y - dividePoint2.Y, 2));//дистанція між точками, де поділяється відрізок і одночасно сторона квадрата, що добудовується
                        if (Math.Abs(dividePoint2.Y - dividePoint1.Y) < 0.0000001)//кутовий коефіцієнт прямої, що проходить через точки поділу(//відрізок, що поділяється - горизонтальний)
                        {
                            if(dividePoint1.Y < centerY)//відрізок вище за центр, тому добудовуємо квадрат вгору
                            {
                                pointsTemp.Add(new Point(dividePoint1.X, dividePoint1.Y - distanceBetweenDividePoints));
                                pointsTemp.Add(new Point(dividePoint2.X, dividePoint2.Y - distanceBetweenDividePoints));
                            }
                            else//відрізок нижче за центр, тому добудовуємо квадрат вниз
                            {
                                pointsTemp.Add(new Point(dividePoint1.X, dividePoint1.Y + distanceBetweenDividePoints));
                                pointsTemp.Add(new Point(dividePoint2.X, dividePoint2.Y + distanceBetweenDividePoints));
                            }
                        } else//відрізок, що поділяється - вертикальний
                        {

                            if (dividePoint1.X < centerX)//відрізок лівіше від центра, тому добудовуємо квадрат вліво
                            {
                                pointsTemp.Add(new Point(dividePoint1.X - distanceBetweenDividePoints, dividePoint1.Y));
                                pointsTemp.Add(new Point(dividePoint2.X - distanceBetweenDividePoints, dividePoint2.Y));
                            }
                            else//відрізок правіше від центра, тому добудовуємо квадрат вправо
                            {
                                pointsTemp.Add(new Point(dividePoint1.X + distanceBetweenDividePoints, dividePoint1.Y));
                                pointsTemp.Add(new Point(dividePoint2.X + distanceBetweenDividePoints, dividePoint2.Y));
                            }
                        }
                        break;
                }
                pointsTemp.Add(dividePoint2);
            }
            CurIterationPoints = pointsTemp;
            steps.Add(CurIterationPoints);
        }


        private void AddIterations()
        {
            if(CurIteration >= 0)
            {
                for (int i = 0; i < IterationsToAddCount; ++i)
                {
                    ++CurIteration;
                    FractalIteration();
                }
            }
        }

        private void SubstractIterations()
        {
            for(int i = 0; i < IterationsToSubstractCount; ++i)
            {
                if(steps.Count > 0)
                {
                    steps.RemoveAt(CurIteration);
                    --CurIteration;
                }
            }
            if(steps.Count > 0)
            {
                CurIterationPoints = steps.Last();
            } else
            {
                CurIterationPoints = new PointCollection();
            }
        }

    }
}
