﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace Fractal_images.Converters
{
    internal class RGBConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if (value != null)
            {
                Color color = (Color)value;
                string res = color.R.ToString()+"; "+color.G.ToString()+"; "+color.B.ToString();
                return res;
            }
            return "converter failed";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string s = value as string;
            if (value != null)
            {
                string[] subs = s.Split("; ");
                Color rgb = new Color();
                rgb.R = Byte.Parse(subs[0]);
                rgb.G = Byte.Parse(subs[1]);
                rgb.B = Byte.Parse(subs[2]);
                return rgb;
            }
            return "converter failed";
        }
    }
}
