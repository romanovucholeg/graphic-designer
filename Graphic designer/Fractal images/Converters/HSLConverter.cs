﻿using Fractal_images.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Fractal_images.Converters
{
    internal class HSLConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {

            if (value != null)
            {
                HSLColor color = (HSLColor)value;
                string res = color.H.ToString("F") + "; " + color.S.ToString("F") + "; " + color.L.ToString("F");
                return res;
            }
            return "converter failed";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string s = value as string;
            if (value != null)
            {
                string[] subs = s.Split("; ");
                HSLColor hsl = new HSLColor(Double.Parse(subs[0]), Double.Parse(subs[1]), Double.Parse(subs[2]));
                return hsl;
            }
            return "converter failed";
        }
    }
}
