﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;

namespace Fractal_images.Converters
{
    internal class AxialPointConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value != null)
            {
                ComboBoxItem comboBoxItem = (ComboBoxItem)value;
                int intValue = Int32.Parse((string)comboBoxItem.Content);
                return intValue - 1;
            }
            return "converter failed";
        }
    }
}
