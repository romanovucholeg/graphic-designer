﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fractal_images.Models
{
    internal class HSLColor
    {
        public HSLColor(double h, double s, double l)
        {
            H= h;
            S= s;
            L= l;
        }


        public double H { get; set; }

        public double S { get; set; }
        public double L { get; set; }

    }
}
