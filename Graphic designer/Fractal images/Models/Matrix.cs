﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Fractal_images.Models
{
    internal class Matrix
    {
        double [,] matrix;

        public Matrix() { matrix = new double[,] { { 0, 0, 0}, { 0, 0, 0 }, { 0, 0, 0 } }; }
        public Matrix(int rows, int columns) { matrix = new double[rows, columns]; }


        public Matrix(double x1, double y1, double x2, double y2, double x3, double y3, double x4, double y4)
        {
            matrix = new double[,] { { x1, y1, 1 }, { x2, y2, 1 }, { x3, y3, 1 }, {x4, y4, 1 } };
        }

        public Matrix(double x1, double y1, double z1, double x2, double y2, double z2, double x3, double y3, double z3)
        {
            matrix = new double[,] { { x1, y1, z1 }, { x2, y2, z2 }, { x3, y3, z3 } };
        }

        public Matrix(PointCollection points)
        {
            matrix = new double[,] { { points.ElementAt(0).X, points.ElementAt(0).Y, 1 } , { points.ElementAt(1).X, points.ElementAt(1).Y, 1 },
                { points.ElementAt(2).X, points.ElementAt(2).Y, 1 }, { points.ElementAt(3).X, points.ElementAt(3).Y, 1 } };
        }

        public static Matrix operator*(Matrix firstMatrix, Matrix secondMatrix)
        {
            Matrix result = new Matrix(firstMatrix.matrix.GetLength(0), firstMatrix.matrix.GetLength(1));
            for(int i = 0; i < firstMatrix.matrix.GetLength(0); i++)
            {
                for( int j = 0; j < firstMatrix.matrix.GetLength(1); j++)
                {
                    result.matrix[i, j] = firstMatrix.matrix[i, 0]*secondMatrix.matrix[0, j]+ firstMatrix.matrix[i, 1] * secondMatrix.matrix[1, j] + 
                        firstMatrix.matrix[i, 2] * secondMatrix.matrix[2, j];
                }
            }
            return result;
        }

        public void Print()
        {
            for(int i = 0;i < matrix.GetLength(0); i++)
            {
                for (int j = 0;j < matrix.GetLength(1); j++) {
                    Console.Write(matrix[i, j]+"\t");
            }
                Console.WriteLine();
                }
        }

        public PointCollection getPoints()
        {
            PointCollection points = new PointCollection();
            for(int i = 0; i < matrix.GetLength(0); ++i)
            {
                points.Add(new Point(matrix[i, 0], matrix[i, 1]));
            }
            return points;
        }
    }
}
