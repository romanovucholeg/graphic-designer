﻿using Fractal_images.Commands;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Fractal_images.Models
{
    internal class CustomImage : ModelBase
    {
        private Color rgbColor = Color.FromArgb(255, 255, 255, 255);
        public Color RgbColor
        {
            get { return rgbColor; }
            set
            {
                rgbColor = value;
                RgbToHls(RgbColor, hslColor);
                OnPropertyChanged(nameof(HslColor));
                OnPropertyChanged(nameof(RgbColor));
            }
        }

        private HSLColor hslColor = new HSLColor(0, 0, 1);
        public HSLColor HslColor
        {
            get { return hslColor; }
            set
            {
                hslColor = value;
                rgbColor = HslToRgb(rgbColor, HslColor);
                OnPropertyChanged(nameof(RgbColor));
                OnPropertyChanged(nameof(HslColor));
            }
        }


        private Image image;
        public Image Image
        {
            get { return image; }
            set
            {
                image = value;
                OnPropertyChanged(nameof(Image));
            }
        }

        private double saturation = 1;
        public double Saturation
        {
            get { return saturation; }
            set
            {
                saturation = value;
                OnPropertyChanged(nameof(Saturation));
            }
        }

        private BitmapImage bitmapImage;
        public BitmapImage BitmapImage
        {
            get { return bitmapImage; }
            set
            {
                bitmapImage = value;
                OnPropertyChanged(nameof(BitmapImage));
            }
        }

        private int imageWidth;
        public int ImageWidth
        {
            get { return imageWidth; }
            set
            {
                imageWidth = value;
                OnPropertyChanged(nameof(ImageWidth));
            }
        }

        private int imageHeight;
        public int ImageHeight
        {
            get { return imageHeight; }
            set
            {
                imageHeight = value;
                OnPropertyChanged(nameof(ImageHeight));
            }
        }

        private Uri imagePath;
        public Uri ImagePath
        {
            get { return imagePath; }
            set
            {
                imagePath = value;
                OnPropertyChanged(nameof(ImagePath));
            }
        }
        public CustomImage()
        {
            ImagePath = new Uri(@"..\..\..\Images\nature.jpg", UriKind.Relative);
            BitmapImage = new BitmapImage();
            BitmapImage.BeginInit();
               BitmapImage.UriSource = ImagePath;
            BitmapImage.EndInit();
            Image = new Image();
            Image.Source = BitmapImage;

        }

        public void СhangeSaturation()
        {
            int width = BitmapImage.PixelWidth;
            int height = BitmapImage.PixelHeight;

            int stride = width * 4;
            byte[] pixelData = new byte[stride * height];
            BitmapImage.CopyPixels(pixelData, stride, 0);
            for (int i = 1; i < stride * height; i += 4)
            {
                pixelData[i] = (byte)(Saturation*255);
            }
            PixelFormat pf = PixelFormats.Bgr32;
            BitmapSource changedBitmapImage = BitmapSource.Create(width, height,
    96, 96, pf, null,
    pixelData, stride);
            Image.Source = changedBitmapImage;
            String filePath = @"D:\My\Study\My_3_course\1_term\KG\Labs\Lab_4\Graphic designer\Fractal images\Images\changedImage.jpg";

            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create((BitmapSource)Image.Source));
            using (FileStream stream = new FileStream(filePath, FileMode.Create))
                encoder.Save(stream);
        }


        // Convert an RGB value into an HLS value.
        public static void RgbToHls(Color rgbColor, HSLColor hslColor)
        {
            // Convert RGB to a 0.0 to 1.0 range.
            double double_r = rgbColor.R / 255.0;
            double double_g = rgbColor.G / 255.0;
            double double_b = rgbColor.B / 255.0;

            // Get the maximum and minimum RGB components.
            double max = double_r;
            if (max < double_g) max = double_g;
            if (max < double_b) max = double_b;

            double min = double_r;
            if (min > double_g) min = double_g;
            if (min > double_b) min = double_b;

            double diff = max - min;
            hslColor.L = (max + min) / 2;
            if (Math.Abs(diff) < 0.00001)
            {
                hslColor.S = 0;
                hslColor.H = 0;  // H is really undefined.
            }
            else
            {
                if (hslColor.L <= 0.5) hslColor.S = diff / (max + min);
                else hslColor.S = diff / (2 - max - min);

                double r_dist = (max - double_r) / diff;
                double g_dist = (max - double_g) / diff;
                double b_dist = (max - double_b) / diff;

                if (double_r == max) hslColor.H = b_dist - g_dist;
                else if (double_g == max) hslColor.H = 2 + r_dist - b_dist;
                else hslColor.H = 4 + g_dist - r_dist;

                hslColor.H = hslColor.H * 60;
                if (hslColor.H < 0) hslColor.H += 360;
            }
            
        }

        // Convert an HLS value into an RGB value.
        public static Color HslToRgb(Color rgbColor, HSLColor hslColor)
        {
            double p2;
            if (hslColor.L <= 0.5) p2 =hslColor.L* (1 + hslColor.S);
            else p2 =hslColor.L+ hslColor.S - hslColor.L* hslColor.S;

            double p1 = 2 *hslColor.L- p2;
            double double_r, double_g, double_b;
            if (hslColor.S == 0)
            {
                double_r = hslColor.L;
                double_g = hslColor.L;
                double_b = hslColor.L;
            }
            else
            {
                double_r = QqhToRgb(p1, p2, hslColor.H + 120);
                double_g = QqhToRgb(p1, p2, hslColor.H);
                double_b = QqhToRgb(p1, p2, hslColor.H - 120);
            }

            // Convert RGB to the 0 to 255 range.
            rgbColor.R = (byte)(double_r * 255.0);
            rgbColor.G = (byte)(double_g * 255.0);
            rgbColor.B = (byte)(double_b * 255.0);
            return rgbColor;
        }

        private static double QqhToRgb(double q1, double q2, double hue)
        {
            if (hue > 360) hue -= 360;
            else if (hue < 0) hue += 360;

            if (hue < 60) return q1 + (q2 - q1) * hue / 60;
            if (hue < 180) return q2;
            if (hue < 240) return q1 + (q2 - q1) * (240 - hue) / 60;
            return q1;
        }

        public void LoadImage()
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";
            if (op.ShowDialog() == true)
            {
                ImagePath = new Uri(op.FileName);
                BitmapImage = new BitmapImage(ImagePath);
                BitmapImage.UriSource = ImagePath;
                Image.Source = BitmapImage;
            }

        }

    }
}
