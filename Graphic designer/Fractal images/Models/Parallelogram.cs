﻿using Fractal_images.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Fractal_images.Models
{
    internal class Parallelogram : ModelBase
    {

        public Parallelogram()
        {
            points = new PointCollection(4);
            points.Add(new Point(100, 100));
            points.Add(new Point(110, 200));
            points.Add(new Point(210, 200));
            points.Add(new Point(200, 100));
            Viewport = new Rect(0, 0, 10, 10);
            MoveTimer.Tick += MoveTimer_Tick;
            MoveTimer.Interval = TimeSpan.FromMilliseconds(250);
        }

        private int curTick;

        private bool isScaleChanged = false;

        private System.Windows.Threading.DispatcherTimer MoveTimer = new System.Windows.Threading.DispatcherTimer();

        private PointCollection pointsBeforeMove;

        private List<PointCollection> reductedAtEachStep = new List<PointCollection>(10);

        private Rect viewport;
        public Rect Viewport
        {
            get { return viewport; }
            set
            {
                viewport = value;
                OnPropertyChanged(nameof(Viewport));
            }
        }

        private int pixelScale = 10;
        public int PixelScale
        {
            get { return pixelScale; }
            set
            {
                pixelScale = value;
                isScaleChanged = true;
                OnPropertyChanged(nameof(PixelScale));
            }
        }

        private PointCollection points;
        public PointCollection Points
        {
            get { return points; }
            set
            {
                points = value;
                OnPropertyChanged(nameof(Points));
            }
        }

        private int reductionCoefficient = 4;
        public int ReductionCoefficient
        {
            get { return reductionCoefficient; }
            set
            {
                reductionCoefficient = value;
                OnPropertyChanged(nameof(ReductionCoefficient));
            }
        }

        private int axialPoint = 0;
        public int AxialPoint
        {
            get { return axialPoint; }
            set
            {
                axialPoint = value;
                OnPropertyChanged(nameof(AxialPoint));
            }
        }


        private DelegateCommand startMoveCommand;

        public ICommand StartMoveCommand
        {
            get
            {
                if (startMoveCommand == null)
                {

                    startMoveCommand = new DelegateCommand(StartMove);
                }
                return startMoveCommand;
            }
        }



        private void MoveTimer_Tick(object? sender, EventArgs e)
        {
            if(curTick < 10)
            {
                //матриця переміщення осьової точки на початок координат
                Matrix moveAxialPointToStartMatrix = new Matrix(1, 0, 0, 0, 1, 0, (-1.0) * reductedAtEachStep.ElementAt(curTick).ElementAt(AxialPoint).X,
                    (-1.0) * reductedAtEachStep.ElementAt(curTick).ElementAt(AxialPoint).Y, 1);
                Matrix centerAtStartMatrix = new Matrix(reductedAtEachStep.ElementAt(curTick));
                Matrix axialPointAtStartMatrix = centerAtStartMatrix * moveAxialPointToStartMatrix;
                //матриця повороту на 1/10 від 360 градусів
                Matrix rotationMatrix = new Matrix(Math.Cos(Math.PI*2/10*(curTick+1)), (-1)*Math.Sin(Math.PI * 2 / 10 * (curTick + 1)), 0, 
                    Math.Sin(Math.PI * 2 / 10 * (curTick + 1)), Math.Cos(Math.PI * 2 / 10 * (curTick + 1)), 0, 0, 0, 1);
                Matrix rotatedMatrix = axialPointAtStartMatrix*rotationMatrix;
                //матриця переміщення на початкову позицію
                Matrix moveToStartPositionMatrix = new Matrix(1, 0, 0, 0, 1, 0,  pointsBeforeMove.ElementAt(axialPoint).X,
                     pointsBeforeMove.ElementAt(axialPoint).Y, 1);
                Matrix finalMatrix = rotatedMatrix * moveToStartPositionMatrix;
                Points = finalMatrix.getPoints();
                if(isScaleChanged)
                {
                    PointCollection nextPoints = new PointCollection();
                    for (int i = 0; i < Points.Count; i++)
                    {
                        nextPoints.Add(new Point(Points.ElementAt(i).X * pixelScale / 10, Points.ElementAt(i).Y * pixelScale / 10));
                    }
                    Points = nextPoints;
                }
                ++curTick;
            } else
            {
                isScaleChanged = false;
                curTick = 0;
                MoveTimer.Stop();
                reductedAtEachStep.Clear();
            }
        }

        public void StartMove()
        {
            ReductTenTimes();
            pointsBeforeMove = Points;
            curTick = 0;
            MoveTimer.Start();
        }

        public void ReductTenTimes()
        {
            Matrix matrixBeforeMovement = new Matrix(Points.ElementAt(0).X, Points.ElementAt(0).Y, Points.ElementAt(1).X, Points.ElementAt(1).Y, Points.ElementAt(2).X, 
                Points.ElementAt(2).Y, Points.ElementAt(3).X, Points.ElementAt(3).Y);
            Point center = new Point((Points.ElementAt(0).X + Points.ElementAt(2).X) / 2.0, (Points.ElementAt(0).Y + Points.ElementAt(2).Y) / 2.0);//центр паралелограма
            //матриця переміщення в початок координат
            Matrix moveToStartMatrix = new Matrix(1, 0, 0, 0, 1, 0, (-1.0)*center.X, (-1.0) * center.Y, 1);
            //переміщення в початок координат
            Matrix matrixAtStart = matrixBeforeMovement*moveToStartMatrix;
            //MessageBox.Show(matrixAtStart.getPoints().ToString());
            //кінцевий коефіцієнт 
            double lastCoefficient = 1.0 / reductionCoefficient;
            //крок з яким зменшуємо об'єкт на кожному кроці. Наприклад якщо треба зменшити в 2 рази, то крок буде 0.1(бо 10 ітерацій)
            double step = (1 - lastCoefficient) / 10;
            for (int i = 1; i < 11; ++i)
            {
                //матриця стиску
                double curReductionCoefficient = 1 - step * i;
                Matrix reductionMatrix = new Matrix(curReductionCoefficient, 0, 0, 0, curReductionCoefficient, 0, 0, 0, 1);
                //стиснення
                Matrix resultMatrix = matrixAtStart * reductionMatrix;
                PointCollection curResultPoints = new PointCollection(resultMatrix.getPoints());
                reductedAtEachStep.Add(curResultPoints);
            }
        }

    }
}
